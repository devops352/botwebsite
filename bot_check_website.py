import os
import multiprocessing
import logging 
import time 
import requests 
import socket

logging.basicConfig(filename='bot_website.log') 
class WebblockException(Exception):
    pass


def ping_website(address, timeout=20):
    try:
        response = requests.head(address,timeout=timeout)
        if response.status_code >= 400:
            logging.warning('Website %s returned status_code=%s'%(address,response.status_code))
            raise WebblockException()
    except requests.exceptions.RequestException:
        logging.warning('Timeout expired for website %s' % address)
        raise WebblockException

def notify_owner(address): 
    logging.warning('Notifying the owner of %s  website' % address) 
    time.sleep(0.5) 

def check_website(address): 
    try:
        print('PID:%s Process name: %s adress tested: %s' %
              (os.getpid(),multiprocessing.current_process().name,address))
        ping_website(address) 
    except WebblockException: 
        notify_owner(address) 


WEBSITE_LIST =[ 'http://envato.com', 'http://amazon.co.uk', 'http://amazon.com', 'http://facebook.com', 'http://google.com', 'http://google.fr', 'http://google.es', 'http://google.co.uk', 'http://internet.org', 'http://gmail.com', 'http://stackoverflow.com', 'http://github.com', 'http://heroku.com', 'http://really-cool-available-domain.com', 'http://djangoproject.com', 'http://rubyonrails.org', 'http://basecamp.com', 'http://trello.com', 'http://yiiframework.com', 'http://shopify.com', 'http://another-really-interesting-domain.co', 'http://airbnb.com', 'http://instagram.com', 'http://snapchat.com', 'http://youtube.com', 'http://baidu.com', 'http://yahoo.com', 'http://live.com', 'http://linkedin.com', 'http://yandex.ru', 'http://netflix.com', 'http://wordpress.com', 'http://bing.com', ]




NUM_WORKERS = 4

start_time = time.time()

with multiprocessing.Pool(processes=NUM_WORKERS) as pool:
    result = pool.map_async(check_website,WEBSITE_LIST)
    result.wait()
    
end_time = time.time()

print('Time for multiprocessing %s secs' % (end_time - start_time))


